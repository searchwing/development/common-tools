import argparse

import geopy.distance
from pymavlink import mavutil
import numpy as np
import geopy

def calculate_energy_per_km(bin_file, start_time, end_time, battery_id=0):
    """
    Calculate Wh/km power needs from an ArduPilot bin file.

    Args:
        bin_file (str): Path to the ArduPilot .bin file.
        start_time (float): Start time (in seconds) relative to log start.
        end_time (float): End time (in seconds) relative to log start.
        battery_id (int): Battery ID to calculate energy consumption for.

    Returns:
        results (dict): Dictionary containing the overall and time-filtered results.
    """
    # Open the bin file
    mlog = mavutil.mavlink_connection(bin_file,dialect='ardupilotmega',robust_parsing=False)

    # Read messages
    results={"overall":{"energy_consumed":0,"distance_traveled":0,"watts":[]},
             "timefilter":{"energy_consumed":0,"distance_traveled":0,"watts":[]}}
    
    prev_lat = None
    prev_lon = None
    prev_time = None

    # Iterate through messages in the log
    while True:
        try:
            msg = mlog.recv_match(blocking=False)
            print(msg)
            if msg is None:
                break

            in_time_filter=False

            # Ensure we are only processing messages with timestamps
            if hasattr(msg, 'TimeUS'):
                time_s = msg.TimeUS / 1e6

                # Process only within the specified time range
                if time_s > start_time and time_s < end_time:
                    in_time_filter = True
                if time_s > end_time:
                    break

                # Process power consumption from the CURR message
                if msg.get_type() == 'BAT' and msg.Inst == battery_id:
                    
                    # Current in Amps and Voltage in Volts
                    current = msg.Curr  # Convert from centiamps to amps
                    voltage = msg.Volt  # Convert from centivolts to volts
                    watt= current*voltage

                    if prev_time is not None:
                        dt = time_s - prev_time
                        energy_consumed = watt * dt  # Ws
                        #print(f"{time_s}:{energy_consumed} ... {msg.EnrgTot}")

                        results["overall"]["watts"].append(watt)
                        results["overall"]["energy_consumed"] += energy_consumed
                    
                        if in_time_filter:
                            results["timefilter"]["watts"].append(watt)
                            results["timefilter"]["energy_consumed"] += energy_consumed
                        

                    prev_time = time_s

                # Process distance traveled from GPS messages
                if msg.get_type() == 'GPS':
                    if prev_lat is not None and prev_lon is not None:
                        lat, lon = msg.Lat , msg.Lng
                        distance = geopy.distance.great_circle((prev_lat, prev_lon), (lat, lon)).km

                        results["overall"]["distance_traveled"] += distance
                        if in_time_filter:
                            results["timefilter"]["distance_traveled"] += distance
                        
                        prev_lat, prev_lon = lat, lon
                    else:
                        prev_lat, prev_lon = msg.Lat , msg.Lng

        except Exception as e:
            print(f"Error reading message: {e}")
            break

    if results["overall"]["distance_traveled"] == 0:
        raise ValueError("No distance traveled in the specified time range.")

    # Calculate J/km
    results["overall"]["energy_per_km"]=results["overall"]["energy_consumed"]/results["overall"]["distance_traveled"]/3600
    results["overall"]["watts"]=np.mean(results["overall"]["watts"])
    results["timefilter"]["energy_per_km"]=results["timefilter"]["energy_consumed"]/results["timefilter"]["distance_traveled"]/3600
    results["timefilter"]["watts"]=np.mean(results["timefilter"]["watts"])
    return results

def main():

    parser = argparse.ArgumentParser(description="Calculate Wh/km power needs from ArduPilot .bin file.")
    parser.add_argument("bin_file", type=str, help="Path to the ArduPilot .bin file.")
    parser.add_argument("start_time", type=float, help="Start time in seconds (delta from log start).")
    parser.add_argument("end_time", type=float, help="End time in seconds (delta from log start).")
    parser.add_argument(
        "-b",
        "--battery_id",
        help="Battery id to calculate energy consumption for.",
        type=int,
        default=0,
    )
    args = parser.parse_args()


    try:
        results = calculate_energy_per_km(args.bin_file, args.start_time, args.end_time, args.battery_id)

        for result in results:
            resultdict = results[result]
            print(f"{result} Distance: {resultdict['distance_traveled']} km")
            print(f"{result} Mean power: {resultdict['watts']} W")
            print(f"{result} Efficiency: {resultdict['energy_per_km']} Wh/km")

    except Exception as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    main()
