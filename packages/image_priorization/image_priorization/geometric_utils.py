from shapely.geometry import Polygon


def calculate_polygon_area(area: Polygon):
    return area.area


def calculate_intersection(poly1: Polygon, poly2: Polygon) -> Polygon:
    """
    Simple utility function to avoid warnings
    """
    if poly1.intersects(poly2):
        return poly1.intersection(poly2)
    else:
        return Polygon([])
