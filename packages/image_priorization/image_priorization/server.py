import argparse
import warnings
from typing import Sequence

try:
    import uvicorn
    from fastapi import FastAPI
except ImportError:
    warnings.warn("Cannot use fastapi client. Install fastapi first.")


from pydantic import BaseModel

from . import AreaCollector, OverlapResult, OverlapStatistics, filter_overlapping_polygons
from .greedy_priorization import filter_images_greedy_algorithm
from .lp_optimization import filter_images_lp_optimization
from .structures import (
    GreedyOptimizationInput,
    LPOptimizationInput,
    PointRest,
    PolygonFilterInputRest,
    convert_polygon_to_shapely,
    convert_polygons_to_shapely,
)

app = FastAPI()


class OverlapStatisticsBody(BaseModel):
    area: Sequence[Sequence[PointRest]]
    polygon: Sequence[PointRest]


@app.put("/filter_polygons", response_model=OverlapResult)
async def filter_polygons(polygon_input: PolygonFilterInputRest):
    shapely_polygons = convert_polygons_to_shapely(polygon_input.polygons)
    out = filter_overlapping_polygons(
        shapely_polygons,
        selection_modus=polygon_input.selection_modus,
        keep_keys=polygon_input.keep_keys,
    )
    return out


@app.put("/calculate_overlap_statistics", response_model=OverlapStatistics)
async def calculate_overlap_statistics(polygon_input: OverlapStatisticsBody):
    area = AreaCollector()
    for polygon in polygon_input.area:
        area.add_area(convert_polygon_to_shapely(polygon))
    return area.calculate_polygon_set_statistics(convert_polygon_to_shapely(polygon_input.polygon))


@app.put("/filter_images_lp_optimization")
async def filter_lp_optimization(input: LPOptimizationInput):
    return filter_images_lp_optimization(
        images=input.images,
        num_images=input.num_images,
        threshold=input.threshold,
        keep_keys=input.keep_keys,
    )


@app.put("/filter_greedy")
async def filter_greedy(input: GreedyOptimizationInput):
    return filter_images_greedy_algorithm(
        images=input.images,
        area_thresh_max=input.area_thresh_max,
        force_overlap=input.force_overlap,
        threshold=input.threshold,
        keep_keys=input.keep_keys,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Start uvicorn server")
    parser.add_argument("--host", type=str, default="0.0.0.0")
    parser.add_argument("--port", type=int, default=5000)
    args = parser.parse_args()

    uvicorn.run("image_priorization.server:app", host=args.host, port=args.port, workers=1)
