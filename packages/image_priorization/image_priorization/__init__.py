from typing import Any, Dict, List, Optional

from .structures import (
    LineString,
    NonOverlappingArea,
    OverlapResult,
    OverlapStatistics,
    Point,
    Polygon,
    SelectionModus,
)


def filter_overlapping_polygons(
    polygons: Dict[Any, Polygon],
    selection_modus: SelectionModus = SelectionModus.STANDARD,
    keep_keys: List[Any] = [],
) -> OverlapResult:
    """
    Filter static
    ToDo: documentation
    """
    area = NonOverlappingArea()
    for key, polygon in polygons.items():
        area.add_object(key, polygon)

    out = OverlapResult(keep=[], remove=[])

    # ToDo, Optimization should be global --> you could then choose, e.g. minimum number of images!
    key_indices = list(polygons.keys())

    def get_area(key):
        return polygons[key].area

    if selection_modus == SelectionModus.SMALLEST_FIRST:
        key_indices = sorted(key_indices, key=get_area)
    elif selection_modus == SelectionModus.LARGEST_FIRST:
        key_indices = sorted(key_indices, key=get_area, reverse=True)
    elif selection_modus == SelectionModus.STANDARD:
        pass
    else:
        raise ValueError("Provide correct modus for removale")

    for key in key_indices:
        if key not in keep_keys and area.is_removable(key):
            area.remove_object(key)
            out.remove.append(key)
        else:
            out.keep.append(key)
    return out


class AreaCollector:
    """
    ToDo Documentation: for easy calculation of new image
    """

    def __init__(self) -> None:
        self.merged_area: Optional[Polygon] = None

    def calculate_polygon_set_statistics(self, polygon: Polygon) -> OverlapStatistics:
        from .geometric_utils import calculate_intersection, calculate_polygon_area

        if self.merged_area is None:
            raise ValueError("Add Polygons first")

        # avoid intersection warning here
        inter = calculate_intersection(polygon, self.merged_area)

        is_touching = False
        if inter.area > 0 or isinstance(inter, Point) or isinstance(inter, LineString):
            is_touching = True

        return OverlapStatistics(
            overlap_area=inter.area,
            non_overlap_area=polygon.area - inter.area,
            is_touching=is_touching,
            overlap_area_geo=calculate_polygon_area(inter),
            non_ovlerap_area_geo=calculate_polygon_area(polygon - inter),
        )

    def add_area(self, polygon: Polygon):
        if self.merged_area is None:
            self.merged_area = polygon
        self.merged_area = self.merged_area.union(polygon)
