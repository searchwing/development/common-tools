from typing import Any, Dict, List, Tuple

import pandas as pd

from .geometric_utils import calculate_intersection, calculate_polygon_area
from .structures import ImageWithDetections, convert_polygon_to_shapely


def convert_images_to_pandas(images: Dict[Any, ImageWithDetections]) -> pd.DataFrame:
    df_images = pd.DataFrame.from_records(
        [
            {
                "image_area": i.image_area,
                "detections": i.detections,
                "datetime_original": i.datetime_original,
            }
            for i in images.values()
        ],
        index=list(images.keys()),
    )
    df_images["polygon"] = df_images.apply(
        lambda row: convert_polygon_to_shapely(row["image_area"]), axis=1
    )
    df_images["max_det_threshold"] = df_images.apply(
        lambda row: max(row["detections"].scores) if len(row["detections"].scores) > 0 else 0,
        axis=1,
    )
    return df_images


def check_image(cur_last_image, to_test, area_thresh_max) -> Tuple[bool, bool]:
    area = calculate_polygon_area(cur_last_image["polygon"])
    if area is None or area == 0:
        area = 1e-16
    inter = calculate_intersection(cur_last_image["polygon"], to_test["polygon"])
    inter_area = calculate_polygon_area(inter)
    if inter_area is None:
        inter_area = 0
    relative_overlap = inter_area / area
    is_overlapping = relative_overlap > 0
    is_overlap_smaller_thres = relative_overlap < area_thresh_max
    return is_overlapping, is_overlap_smaller_thres


def filter_images_greedy_algorithm(
    images: Dict[Any, ImageWithDetections],
    keep_keys: List[Any] = [],
    threshold: float = 0.9,
    area_thresh_max: float = 0.05,
    force_overlap: bool = False,
) -> List[Any]:
    """
    Algorithm:
        1. Sort images by increasing timestamp
        2. Take first image area and set as last image to keep
        3. Take next image area after last image and calc overlap with last image
        4. if overlap < treshold (e.g. 10%) or detection_prob > treshold: set as last image to keep
        5. Jump to step 2
    """

    images_df = convert_images_to_pandas(images)

    # add keep images (e.g. which are already seen)
    images_df["keep"] = False
    images_df.loc[keep_keys, "keep"] = True

    images_selected_greedy = []
    images_df = images_df.sort_values("datetime_original")
    last_image_selected = images_df.loc[images_df.index[0]]
    last_overlapping_image = None
    images_selected_greedy.append(images_df.index[0])
    n = 1
    while n < len(images_df):
        cur_index = images_df.index[n]
        cur_row = images_df.iloc[n]
        if cur_row["keep"]:
            last_image_selected = cur_row
            images_selected_greedy.append(cur_index)
            last_overlapping_image = None
        elif (cur_row["max_det_threshold"]) >= threshold:
            last_image_selected = cur_row
            images_selected_greedy.append(cur_index)
            last_overlapping_image = None
        else:
            is_overlapping, is_overlap_smaller_thres = check_image(
                last_image_selected, cur_row, area_thresh_max
            )
            if force_overlap:
                if is_overlap_smaller_thres and is_overlapping:
                    last_image_selected = cur_row
                    images_selected_greedy.append(cur_index)
                    last_overlapping_image = None
                elif is_overlap_smaller_thres and not is_overlapping:
                    if last_overlapping_image is None:
                        last_image_selected = cur_row
                        images_selected_greedy.append(cur_index)
                    else:
                        last_image_selected = last_overlapping_row
                        images_selected_greedy.append(last_overlapping_image)
                        last_overlapping_image = None
                        n -= 1
                else:
                    last_overlapping_row = cur_row
                    last_overlapping_image = cur_index

            else:
                if is_overlap_smaller_thres:
                    last_image_selected = cur_row
                    images_selected_greedy.append(cur_index)
        n += 1

    return images_selected_greedy
