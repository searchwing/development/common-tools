from collections import defaultdict
from itertools import combinations
from tkinter import Image
from typing import Any, Dict, List, Sequence

from pulp import LpBinary, LpMaximize, LpProblem, LpStatus, LpVariable, lpSum, value

from .geometric_utils import calculate_polygon_area
from .structures import ImageWithDetections, NonOverlappingArea, convert_polygon_to_shapely


def filter_images_lp_optimization(
    images: Dict[Any, ImageWithDetections],
    num_images: int,
    keep_keys: List[Any] = [],
    threshold: float = 0.9,
) -> Sequence[Any]:
    """
    Filter images based on solvidng an lp-optimization problemn.
    The algorithm tries to find num_images out of images, which are maximizing the covered are of the images.
    In addition the following images are kept:
        * contained in keep_keys
        * images with any detection with any object with a score > threshold
    Internally an LP-optimization problem is created. It combines the covered area by the images.
    The algorithm can handle approximately up to 200 images sufficiently.
    Arguments:
        images: a dict containing the images
        num_images: how many images are kept
        keep_keys: the image-keys that have to be kept
        threshold: object-score threshold
    Returns:
        A list of key which images should be kept
    """
    polygons_shapely = {k: convert_polygon_to_shapely(v.image_area) for k, v in images.items()}

    # extend keep_keys list
    keep_keys_extended = [
        k
        for k, v in images.items()
        if (max(v.detections.scores) > threshold if len(v.detections.scores) > 0 else False)
    ]
    keep_keys_extended.extend(keep_keys)

    non_overlaping_area = NonOverlappingArea()
    for k, v in polygons_shapely.items():
        non_overlaping_area.add_object(k, v)

    # create optimization problem

    prob = LpProblem("AreaMaximationProblem", LpMaximize)

    final_objective_sum = defaultdict(lambda: 0.0)
    c = 500
    variables = {}
    for k, v in polygons_shapely.items():
        final_objective_sum[k] = calculate_polygon_area(v)
        variables[k] = LpVariable(str(k), 0, 1, cat=LpBinary)

    # constraint the num images
    prob += lpSum([v for v in variables.values()]) == num_images

    # constrain overlapping areas
    for k, v in non_overlaping_area.non_overlapping_objects.items():
        if len(v.connected_keys) > 1:
            area = calculate_polygon_area(v.polygon)
            for i, j in combinations(v.connected_keys, 2):
                tmp = [i, j]
                tmp.sort()
                key_com = "_".join([str(i) for i in tmp])
                final_objective_sum[key_com] = final_objective_sum[key_com] - area
                if key_com not in variables:
                    variables[key_com] = LpVariable(str(key_com), 0, 1, cat=LpBinary)
                    prob += lpSum([variables[i], variables[j], -c * variables[key_com]]) >= 2 - c
                    prob += (
                        lpSum([c * (1 - variables[i]), c * (1 - variables[j]), variables[key_com]])
                        >= 1
                    )

    # add constant
    for k in keep_keys_extended:
        prob += variables[k] == 1

    # create objective
    prob += lpSum([v * final_objective_sum[k] for k, v in variables.items()])

    status = prob.solve()
    assert LpStatus[status] == "Optimal"

    return [k for k in images if value(variables[k])]
