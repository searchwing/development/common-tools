from threading import Lock

import pandas as pd
from shapely import MultiPolygon, simplify, unary_union
from shapely.geometry import Polygon

from image_priorization.geometric_utils import calculate_intersection
from image_priorization.greedy_priorization import convert_images_to_pandas
from image_priorization.structures import ImageWithDetections


class PrioretizedQueueGreedy:
    def __init__(
        self, simplification_thres=0, early_stop_prio_thres=0.95, image_prio_cnt_max=50
    ) -> None:
        self._df_queue = None
        self._sent_area_polygon = Polygon()
        self._simplification_thres = simplification_thres
        self._early_stop_prio_thres = early_stop_prio_thres
        self._image_prio_cnt_max = image_prio_cnt_max
        self._lock = Lock()

    def add_image(self, new_image: ImageWithDetections) -> None:
        df_new_image = convert_images_to_pandas({new_image.datetime_original: new_image})
        df_new_image["sent"] = False
        df_new_image["prio"] = 0.0
        if self._df_queue is None:
            self._df_queue = df_new_image
        else:
            with self._lock:
                self._df_queue = pd.concat([self._df_queue, df_new_image])  # todo optimize
            self._prioretize()

    def _prioretize(self) -> None:
        with self._lock:
            # Reset old prioritization
            self._df_queue["prio"] = 0.0
            # Only prio not sent images
            df_not_sent = self._df_queue[self._df_queue["sent"] == False]
            i = 0
            # Iterate through images reversed / from newest to oldest
            for index in range(len(df_not_sent) - 1, -1, -1):
                image = df_not_sent.iloc[index]
                # geometrical_prio = (UnionArea(NewImage,AlreadyTransmittedImages)-AlreadyTransmittedImagesCoveredArea) / UnionArea(NewImage,AlreadyTransmittedImages)
                image_and_sent_union_area = image["polygon"].union(self._sent_area_polygon).area
                if image_and_sent_union_area > 0.0:
                    geometrical_priority = (
                        image_and_sent_union_area - self._sent_area_polygon.area
                    ) / image_and_sent_union_area
                else:
                    geometrical_priority = 1.0

                self._df_queue.at[image.datetime_original, "prio"] = geometrical_priority

                # Early abort prioretization as we only need the next best image to be transmitted
                i = i + 1
                if geometrical_priority > self._early_stop_prio_thres:
                    break
                if i > self._image_prio_cnt_max:
                    break

    def get_image_highest_prio(self) -> ImageWithDetections:
        with self._lock:
            if self._df_queue is None:
                return None
            df_not_sent = self._df_queue[self._df_queue["sent"] == False]
            if len(df_not_sent) == 0:
                return None
            return df_not_sent.loc[df_not_sent["prio"].idxmax()]

    def set_image_sent(self, sent_image: ImageWithDetections) -> None:
        with self._lock:
            if self._df_queue is None:
                return

            # Set sent
            self._df_queue.at[sent_image.datetime_original, "sent"] = True
            self._df_queue.at[sent_image.datetime_original, "prio"] = 0.0

            # Simplify polygon if necessary
            if (
                self._simplification_thres > 0
                and len(self._df_queue[self._df_queue["sent"] == True]) % 10 == 0
                and self._sent_area_polygon.area > 0.0
            ):
                self._sent_area_polygon = simplify(
                    self._sent_area_polygon, tolerance=self._simplification_thres
                )

            try:
                # Add sent_image polygon to sent_polygon
                self._sent_area_polygon = unary_union(
                    [
                        self._sent_area_polygon,
                        self._df_queue.at[sent_image.datetime_original, "polygon"],
                    ]
                )

                # filter detached polygons as they cost quite performance
                if isinstance(self._sent_area_polygon, MultiPolygon):
                    polygons = self._sent_area_polygon.geoms
                    self._sent_area_polygon = max(
                        polygons, key=lambda polygon: polygon.area
                    )  # keep only polygon with highest area
            except:
                pass
        self._prioretize()
        return

    def get_queue(self) -> pd.DataFrame:
        return self._df_queue

    def get_sent_polygon(self) -> Polygon:
        return self._sent_area_polygon
