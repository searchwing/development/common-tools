import math
import uuid
from copy import deepcopy
from datetime import datetime
from enum import Enum
from typing import Any, Dict, Hashable, List, Sequence, Set, Tuple

from pydantic import BaseModel
from shapely.geometry import LineString, Point, Polygon
from shapely.geometry.collection import GeometryCollection
from shapely.ops import unary_union
from shapely.validation import make_valid

from .geometric_utils import calculate_intersection


class Area:
    def __init__(self, polygon: Polygon, connected_key: Any) -> None:
        self.polygon = polygon
        self.connected_keys: Set[Any] = set([connected_key])

    def is_connected(self, key: Any) -> bool:
        return key in self.connected_keys

    def connected_count(self) -> int:
        return len(self.connected_keys)


class NonOverlappingArea:
    def __init__(self) -> None:
        self.objects: Dict[Any, Polygon] = {}
        self.non_overlapping_objects: Dict[str, Area] = {}

    def add_object(self, key: Any, polygon: Polygon):
        if key in self.objects:
            raise ValueError("Key already provided")
        self.objects[key] = polygon
        cur_object = polygon
        objects_to_add = []
        objects_to_remove = []
        for non_over_key, obj in self.non_overlapping_objects.items():
            inter = self.clean_geometric_object(calculate_intersection(polygon, obj.polygon))
            if inter.area > 0:
                try:
                    cur_object = self.clean_geometric_object(
                        cur_object.difference(make_valid(inter))
                    )
                except:
                    print("test")
                obj.polygon = self.clean_geometric_object(obj.polygon.difference(inter))
                inter_area = Area(inter, key)
                inter_area.connected_keys = inter_area.connected_keys.union(
                    deepcopy(obj.connected_keys)
                )
                objects_to_add.append(inter_area)
                if obj.polygon.area < 1e-10:
                    objects_to_remove.append(non_over_key)
            if cur_object.area == 0:
                break

        if cur_object.area > 0:
            objects_to_add.append(Area(cur_object, key))

        for obj in objects_to_add:
            # todo easier would be e.g. a counter, but we do something fancy
            new_key = uuid.uuid4().hex
            if new_key in self.non_overlapping_objects:
                raise ValueError("We should never be here!")
            self.non_overlapping_objects[new_key] = obj

        for obj_key in objects_to_remove:
            self.non_overlapping_objects.pop(obj_key)

    def is_removable(self, key: Any) -> bool:
        return all(
            [
                obj.connected_count() > 1
                for obj in self.non_overlapping_objects.values()
                if obj.is_connected(key)
            ]
        )

    def remove_object(self, key: Any):
        for obj in [obj for obj in self.non_overlapping_objects.values() if obj.is_connected(key)]:
            obj.connected_keys.remove(key)
            if obj.connected_count() == 0:
                raise ValueError("Could not remove objected entirely")
        self.objects.pop(key)

    @staticmethod
    def clean_geometric_object(geom):
        if isinstance(geom, GeometryCollection):
            new_object = [i for i in geom.geoms if isinstance(i, Polygon)]
            return make_valid(unary_union(new_object))
        else:
            return make_valid(geom)


class SelectionModus(Enum):
    STANDARD = "STANDARD"
    SMALLEST_FIRST = "SMALLEST_FIRST"
    LARGEST_FIRST = "LARGEST_FIRST"


class OverlapStatistics(BaseModel):
    overlap_area: float
    non_overlap_area: float
    is_touching: bool
    overlap_area_geo: float
    non_ovlerap_area_geo: float


class OverlapResult(BaseModel):
    remove: List[Any]
    keep: List[Any]


class PointRest(BaseModel):
    lat: float
    lon: float


class ImageCorners(BaseModel):
    ul: Tuple[float, float, float]
    ur: Tuple[float, float, float]
    ll: Tuple[float, float, float]
    lr: Tuple[float, float, float]

    @staticmethod
    def __point_contains_nan(point: Tuple[float, float, float]) -> bool:
        return any([math.isnan(i) for i in point])

    def get_points_list(self) -> Sequence[Tuple[float, float, float]]:
        return [self.ul, self.ur, self.lr, self.ll]

    def is_valid(self) -> bool:
        return all([not self.__point_contains_nan(point) for point in self.get_points_list()])


class PolygonFilterInputRest(BaseModel):
    polygons: Dict[Any, Sequence[PointRest]]
    selection_modus: SelectionModus = SelectionModus.STANDARD
    keep_keys: Sequence[Any] = []


class PolygonFilterInput(BaseModel):
    polygons: Dict[Any, Any]
    selection_modus: SelectionModus = SelectionModus.STANDARD
    keep_keys: Sequence[Any] = []

    def convert_to_rest(self) -> PolygonFilterInputRest:
        return PolygonFilterInputRest(
            polygons=convert_shapely_to_polygons(self.polygons),
            selection_modus=self.selection_modus,
            keep_keys=self.keep_keys,
        )


def convert_polygons_to_shapely(polygons: Sequence[Sequence[PointRest]]) -> Dict[Hashable, Polygon]:
    return {k: convert_polygon_to_shapely(polygon=polygon) for k, polygon in polygons.items()}


def convert_polygon_to_shapely(polygon: Sequence[PointRest]) -> Polygon:
    return Polygon([(point.lon, point.lat) for point in polygon])


def convert_shapely_to_polygons(
    polygons: Dict[Hashable, Polygon]
) -> Dict[Hashable, List[PointRest]]:
    return {k: convert_shapely_to_polygon(polygon) for k, polygon in polygons.items()}


def convert_shapely_to_polygon(polygon: Polygon) -> List[PointRest]:
    return [PointRest(lon=v[0], lat=v[1]) for v in polygon.exterior.coords]


def convert_imagecrns_to_shapely(imagecrns: ImageCorners):
    return Polygon(
        [
            (point[0], point[1])
            for point in [imagecrns.ul, imagecrns.ur, imagecrns.lr, imagecrns.ll, imagecrns.ul]
        ]
    )


class TestUtilInputOutput(BaseModel):
    __test__ = False
    filter_input: PolygonFilterInput
    filter_output: OverlapResult


class Detection(BaseModel):
    scores: Sequence[float] = []
    categories: Sequence[int] = []
    boxes: Sequence[Sequence[float]] = []


class ImageWithDetections(BaseModel):
    image_area: Sequence[PointRest]
    detections: Detection = Detection()
    datetime_original: datetime


class LPOptimizationInput(BaseModel):
    images: Dict[Any, ImageWithDetections]
    keep_keys: Sequence[Any]
    num_images: int
    threshold: float


class GreedyOptimizationInput(BaseModel):
    images: Dict[Any, ImageWithDetections]
    keep_keys: Sequence[Any] = []
    threshold: float = 0.9
    area_thresh_max: float = 0.05
    force_overlap: bool = False
