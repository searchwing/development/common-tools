import time
from datetime import datetime, timedelta

import pytest
from shapely import Polygon
from shapely.ops import unary_union

from image_priorization.queue_greedy_priorization import PrioretizedQueueGreedy
from image_priorization.structures import Detection, ImageWithDetections, PointRest


def create_rectangle(x, y, w, h):
    return [
        PointRest(lat=x, lon=y),
        PointRest(lat=x, lon=y + h),
        PointRest(lat=x + w, lon=y + h),
        PointRest(lat=x + w, lon=y),
    ]


queue_test_images = [
    ImageWithDetections(
        image_area=create_rectangle(0, 0, 1, 1),
        datetime_original=datetime(1, 1, 2),
    ),
    ImageWithDetections(
        image_area=create_rectangle(0, 0.5, 1, 1),
        datetime_original=datetime(1, 1, 3),
    ),
    ImageWithDetections(
        image_area=create_rectangle(0, 1, 1, 1),
        datetime_original=datetime(1, 1, 4),
    ),
    ImageWithDetections(
        image_area=create_rectangle(0, 1.5, 1, 1),
        datetime_original=datetime(1, 1, 5),
    ),
    ImageWithDetections(
        image_area=create_rectangle(0, 2.0, 1, 1),
        datetime_original=datetime(1, 1, 6),
    ),
]


def test_queue_init():
    prioretizer = PrioretizedQueueGreedy()
    for image in queue_test_images:
        prioretizer.add_image(image)
    queue = prioretizer.get_queue()
    assert len(queue) == 5


def test_queue_set_sent():
    prioretizer = PrioretizedQueueGreedy()
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer.set_image_sent(queue_test_images[0])
    queue = prioretizer.get_queue()
    assert queue.iloc[0]["sent"] == True


def test_queue_prioretize():
    prioretizer = PrioretizedQueueGreedy()
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert list(queue["prio"]) == [0, 0, 0, 0, 1]


def test_queue_prioretize_get_image():
    prioretizer = PrioretizedQueueGreedy(early_stop_prio_thres=2.0)
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert list(queue["prio"]) == [1, 1, 1, 1, 1]
    assert (
        queue_test_images[0].datetime_original
        == prioretizer.get_image_highest_prio()["datetime_original"]
    )

    prioretizer = PrioretizedQueueGreedy(early_stop_prio_thres=2.0)
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer.set_image_sent(queue_test_images[0])
    prioretizer.set_image_sent(queue_test_images[2])
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert (
        queue_test_images[4].datetime_original
        == prioretizer.get_image_highest_prio()["datetime_original"]
    )


def test_queue_prioretize_no_early_stop():
    prioretizer = PrioretizedQueueGreedy(early_stop_prio_thres=2.0)
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert list(queue["prio"]) == [1, 1, 1, 1, 1]

    prioretizer = PrioretizedQueueGreedy(early_stop_prio_thres=2.0)
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer.set_image_sent(queue_test_images[0])
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert queue["prio"][0] == pytest.approx(0.0, abs=0.1)
    assert queue["prio"][1] == pytest.approx(1 / 3, abs=0.1)
    assert queue["prio"][2] == pytest.approx(1 / 2, abs=0.1)
    assert queue["prio"][3] == pytest.approx(1 / 2, abs=0.1)
    assert queue["prio"][4] == pytest.approx(1 / 2, abs=0.1)

    prioretizer = PrioretizedQueueGreedy(early_stop_prio_thres=2.0)
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer.set_image_sent(queue_test_images[0])
    prioretizer.set_image_sent(queue_test_images[2])
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert queue["prio"][0] == pytest.approx(0, abs=0.1)
    assert queue["prio"][1] == pytest.approx(0, abs=0.1)
    assert queue["prio"][2] == pytest.approx(0, abs=0.1)
    assert queue["prio"][3] == pytest.approx(1 / 5, abs=0.1)
    assert queue["prio"][4] == pytest.approx(1 / 3, abs=0.1)

    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert queue["prio"][0] == pytest.approx(0, abs=0.1)
    assert queue["prio"][1] == pytest.approx(0, abs=0.1)
    assert queue["prio"][2] == pytest.approx(0, abs=0.1)
    assert queue["prio"][3] == pytest.approx(1 / 5, abs=0.1)
    assert queue["prio"][4] == pytest.approx(1 / 3, abs=0.1)


def test_queue_prioretize_early_stop():
    prioretizer = PrioretizedQueueGreedy(early_stop_prio_thres=0.95)
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert list(queue["prio"]) == [0, 0, 0, 0, 1]


def test_queue_perf():
    import random
    import time

    prioretizer = PrioretizedQueueGreedy(simplification_thres=0.05)
    count = 4000
    images = []
    imgtime = datetime(1, 1, 2)

    for i in range(count):
        if i == count - 1:
            start = time.time()

        imgtime = imgtime + timedelta(0, 1)  # days, seconds, then other fields.
        image = ImageWithDetections(
            image_area=create_rectangle(i * 0.01, i * 0.01, 1, 1),
            datetime_original=imgtime,
        )
        prioretizer.add_image(image)
        images.append(image)
        if i == count - 1:
            end = time.time()
            print("create:{}".format(end - start))
            dur = end - start
            assert dur < 0.05

    sent = random.sample(range(count - 1), count - 1)
    start = time.time()
    i = 0
    for sent_idx in sent:
        if i == len(sent) - 1:
            start = time.time()
        prioretizer.set_image_sent(images[sent_idx])
        if i == len(sent) - 1:
            end = time.time()
            print("set_sent:{}".format(end - start))
            dur = end - start
            assert dur < 0.05
        i = i + 1

    start = time.time()
    prioretizer._prioretize()
    end = time.time()
    print("prio:{}".format(end - start))
    dur = end - start
    assert dur < 0.05


def test_queue_multipoly():
    queue_test_images_multipoly = [
        ImageWithDetections(
            image_area=create_rectangle(0, 0, 1, 1),
            datetime_original=datetime(1, 1, 2),
        ),
        ImageWithDetections(
            image_area=create_rectangle(10, 10.5, 1, 1),
            datetime_original=datetime(1, 1, 3),
        ),
    ]

    prioretizer = PrioretizedQueueGreedy()

    for image in queue_test_images_multipoly:
        prioretizer.add_image(image)

    prioretizer.set_image_sent(queue_test_images_multipoly[0])
    prioretizer.set_image_sent(queue_test_images_multipoly[1])
    assert isinstance(prioretizer.get_sent_polygon(), Polygon)


def test_queue_empty():
    prioretizer = PrioretizedQueueGreedy()
    queue = prioretizer.get_image_highest_prio()
    assert queue is None

    prioretizer = PrioretizedQueueGreedy()
    for image in queue_test_images:
        prioretizer.add_image(image)
    prioretizer.set_image_sent(queue_test_images[0])
    prioretizer.set_image_sent(queue_test_images[1])
    prioretizer.set_image_sent(queue_test_images[2])
    prioretizer.set_image_sent(queue_test_images[3])
    prioretizer.set_image_sent(queue_test_images[4])
    queue = prioretizer.get_image_highest_prio()
    assert queue is None


real_images = [
    ImageWithDetections(
        image_area=[
            PointRest(lat=39.86186981201172, lon=-0.03726997226476669),
            PointRest(lat=39.8575325012207, lon=-0.025984255596995354),
            PointRest(lat=39.848915100097656, lon=-0.031915333122015),
            PointRest(lat=39.85874557495117, lon=-0.04071332514286041),
        ],
        detections=Detection(scores=[], categories=[], boxes=[]),
        datetime_original=datetime(2022, 10, 2, 14, 40, 57, 598732),
    ),
    ImageWithDetections(
        image_area=[
            PointRest(lat=39.869384765625, lon=-0.044429995119571686),
            PointRest(lat=39.86174392700195, lon=-0.036922018975019455),
            PointRest(lat=39.85856628417969, lon=-0.040366146713495255),
            PointRest(lat=39.863525390625, lon=-0.05418602004647255),
        ],
        detections=Detection(scores=[], categories=[], boxes=[]),
        datetime_original=datetime(2022, 10, 2, 14, 40, 57, 764840),
    ),
    ImageWithDetections(
        image_area=[
            PointRest(lat=39.86262512207031, lon=-0.036702875047922134),
            PointRest(lat=39.85834503173828, lon=-0.025469278916716576),
            PointRest(lat=39.850189208984375, lon=-0.031547509133815765),
            PointRest(lat=39.8595085144043, lon=-0.04010183736681938),
        ],
        detections=Detection(scores=[], categories=[], boxes=[]),
        datetime_original=datetime(2022, 10, 2, 14, 41, 0, 592896),
    ),
    ImageWithDetections(
        image_area=[
            PointRest(lat=39.870243072509766, lon=-0.04390626400709152),
            PointRest(lat=39.86250305175781, lon=-0.03629378229379654),
            PointRest(lat=39.85934066772461, lon=-0.039710883051157),
            PointRest(lat=39.86421585083008, lon=-0.05334025248885155),
        ],
        detections=Detection(scores=[], categories=[], boxes=[]),
        datetime_original=datetime(2022, 10, 2, 14, 41, 0, 766747),
    ),
    ImageWithDetections(
        image_area=[
            PointRest(lat=39.86299133300781, lon=-0.0362875871360302),
            PointRest(lat=39.85866165161133, lon=-0.02459033764898777),
            PointRest(lat=39.85026550292969, lon=-0.030852802097797394),
            PointRest(lat=39.859867095947266, lon=-0.03965051472187042),
        ],
        detections=Detection(scores=[], categories=[], boxes=[]),
        datetime_original=datetime(2022, 10, 2, 14, 41, 3, 588612),
    ),
    ImageWithDetections(
        image_area=[
            PointRest(lat=39.870811462402344, lon=-0.04377949610352516),
            PointRest(lat=39.86296081542969, lon=-0.03597470000386238),
            PointRest(lat=39.85979080200195, lon=-0.03934916481375694),
            PointRest(lat=39.86457824707031, lon=-0.05306626856327057),
        ],
        detections=Detection(scores=[], categories=[], boxes=[]),
        datetime_original=datetime(2022, 10, 2, 14, 41, 3, 766818),
    ),
]


def test_queue_prioretize_real():
    prioretizer = PrioretizedQueueGreedy()
    for image in real_images:
        prioretizer.add_image(image)
    prioretizer.set_image_sent(real_images[0])
    prioretizer._prioretize()
    queue = prioretizer.get_queue()
    assert (
        real_images[5].datetime_original
        == prioretizer.get_image_highest_prio()["datetime_original"]
    )
