import time
from datetime import datetime

from shapely.ops import unary_union

from image_priorization.geometric_utils import calculate_polygon_area
from image_priorization.greedy_priorization import filter_images_greedy_algorithm
from image_priorization.structures import ImageWithDetections, PointRest, convert_polygon_to_shapely


def create_rectangle(x, y, w, h):
    return [
        PointRest(lat=x, lon=y),
        PointRest(lat=x, lon=y + h),
        PointRest(lat=x + w, lon=y + h),
        PointRest(lat=x + w, lon=y),
    ]


def test_greedy_filter_datetime():
    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0.5, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1.5, 1, 1, 1),
                datetime_original=datetime(1, 1, 3),
            ),
        },
    )
    assert len(keep) == 2
    assert set(keep) == {2, 3}


def test_greedy_filter_keep():
    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0.5, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1.5, 1, 1, 1),
                datetime_original=datetime(1, 1, 3),
            ),
        },
        keep_keys=[1],
    )
    assert len(keep) == 3
    assert set(keep) == {1, 2, 3}


def test_greedy_filter_detection_threshold():
    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                detections={"scores": [0.99, 0.1]},
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0.5, 1, 1), datetime_original=datetime(1, 1, 1)
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1.5, 1, 1, 1), datetime_original=datetime(1, 1, 3)
            ),
        },
        keep_keys=[],
        threshold=0.5,
    )
    assert len(keep) == 3
    assert set(keep) == {1, 2, 3}

    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                detections={"scores": [0.49, 0.1]},
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0.5, 1, 1), datetime_original=datetime(1, 1, 1)
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1.5, 1, 1, 1), datetime_original=datetime(1, 1, 3)
            ),
        },
        keep_keys=[],
        threshold=0.5,
    )
    assert len(keep) == 2
    assert set(keep) == {2, 3}


def test_greedy_filter_area_thresh():
    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1, 0, 1, 1),
                datetime_original=datetime(1, 1, 3),
            ),
        },
        keep_keys=[1],
        area_thresh_max=1,
    )
    assert len(keep) == 3
    assert set(keep) == {1, 2, 3}

    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1, 0, 1, 1),
                datetime_original=datetime(1, 1, 3),
            ),
        },
        area_thresh_max=0.49,
    )
    assert len(keep) == 1
    assert set(keep) == {2}

    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1.44, 0, 1, 1),
                datetime_original=datetime(1, 1, 3),
            ),
        },
        area_thresh_max=0.05,
    )
    assert len(keep) == 1
    assert set(keep) == {2}


def test_greedy_filter_force_overlap():
    keep = filter_images_greedy_algorithm(
        {
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 2),
            ),
            2: ImageWithDetections(
                image_area=create_rectangle(0.5, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(1.5, 0, 1, 1),
                datetime_original=datetime(1, 1, 3),
            ),
        },
        force_overlap=True,
    )
    assert len(keep) == 3
    assert set(keep) == {1, 2, 3}


def test_example_realistic_dataset(images_with_detections):
    assert len(images_with_detections) > 100

    start = time.time()
    keep_standard = filter_images_greedy_algorithm(images_with_detections)
    stop = time.time()

    time_per_image = (stop - start) / len(images_with_detections)
    assert time_per_image < 1e-2  # ToDo: Do we really want to test time?

    # now we want to filter much less
    keep_almost_all = filter_images_greedy_algorithm(images_with_detections, area_thresh_max=0.99)
    assert len(keep_almost_all) > len(keep_standard)

    keep_force_overlap = filter_images_greedy_algorithm(images_with_detections, force_overlap=True)

    all_area = calculate_polygon_area(
        unary_union(
            [convert_polygon_to_shapely(v.image_area) for v in images_with_detections.values()]
        )
    )
    area_standard = calculate_polygon_area(
        unary_union(
            [
                convert_polygon_to_shapely(images_with_detections[k].image_area)
                for k in keep_standard
            ]
        )
    )
    area_keep_almost_all = calculate_polygon_area(
        unary_union(
            [
                convert_polygon_to_shapely(images_with_detections[k].image_area)
                for k in keep_almost_all
            ]
        )
    )
    area_force_overlap = calculate_polygon_area(
        unary_union(
            [
                convert_polygon_to_shapely(images_with_detections[k].image_area)
                for k in keep_force_overlap
            ]
        )
    )
    assert area_standard < area_keep_almost_all
    assert area_standard < area_force_overlap
    assert area_keep_almost_all / all_area > 0.99
