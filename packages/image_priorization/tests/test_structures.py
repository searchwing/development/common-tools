from image_priorization.structures import Point, Polygon


def test_intersection():
    box1 = Polygon([Point(0, 0), Point(2, 0), Point(2, 2), Point(0, 2)])
    box2 = Polygon([Point(-1, 2), Point(-1, 3), Point(0, 3), Point(1, 1)])
    inter = box1.intersection(box2)

    assert (0.0, 2.0) in [i for i in inter.exterior.coords]
    assert (1.0, 1.0) in [i for i in inter.exterior.coords]
    assert (0.0, 1.5) in [i for i in inter.exterior.coords]
    assert (0.5, 2.0) in [i for i in inter.exterior.coords]


def test_no_intersection():
    box1 = Polygon([Point(0, 0), Point(2, 0), Point(2, 2), Point(0, 2)])
    box2 = Polygon([Point(-1, -1), Point(0, -1), Point(0, 0), Point(-1, 0)])
    inter = box1.intersection(box2)

    assert isinstance(inter, Point)
    assert inter.area == 0

    box1 = Polygon([Point(0, 0), Point(2, 0), Point(2, 2), Point(0, 2)])
    box2 = Polygon([Point(-1, -1), Point(-0.1, -1), Point(-0.1, -0.1), Point(-1, -0.1)])
    inter = box1.intersection(box2)
    assert inter.area == 0
