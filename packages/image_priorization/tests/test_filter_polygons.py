import pytest

from image_priorization import OverlapResult, filter_overlapping_polygons
from image_priorization.structures import TestUtilInputOutput


@pytest.mark.parametrize(
    "data_fixture_name",
    [
        "filter_two_overlapping",
        "filter_two_non_overlapping_polygons",
        "filter_three_overlapping_polygons",
        "filter_standard_remove_modus",
        "filter_largest_first_remove_modus",
        "filter_smallest_first_remove_modus",
        "filter_keep_key",
    ],
)
def test_filter_polygons(data_fixture_name, request):
    input_output: TestUtilInputOutput = request.getfixturevalue(data_fixture_name)
    algo_out = filter_overlapping_polygons(**input_output.filter_input.dict())
    assert isinstance(algo_out, OverlapResult)
    assert set(algo_out.remove) == set(input_output.filter_output.remove)
    assert set(algo_out.keep) == set(input_output.filter_output.keep)
