import random
from datetime import datetime

import pytest
from fastapi.testclient import TestClient
from shapely.geometry import Polygon

from image_priorization.server import app
from image_priorization.structures import (
    ImageWithDetections,
    OverlapResult,
    OverlapStatistics,
    PointRest,
    TestUtilInputOutput,
    convert_shapely_to_polygon,
)


@pytest.fixture
def images():
    n_images = 10
    random.seed(0)
    images = {
        f"{k}": {
            "image_area": create_rectangle(
                random.uniform(0, 10),
                random.uniform(0, 10),
                random.uniform(0.01, 2),
                random.uniform(0.01, 2),
            ),
            "datetime_original": "2010-10-12T12:30",
        }
        for k in range(n_images)
    }

    images["8"]["detections"] = {"scores": [0.99]}
    images["1"]["detections"] = {"scores": [0.1]}
    return images


@pytest.mark.parametrize(
    "data_fixture_name",
    [
        "filter_two_overlapping",
        "filter_two_non_overlapping_polygons",
        "filter_three_overlapping_polygons",
        "filter_standard_remove_modus",
        "filter_largest_first_remove_modus",
        "filter_smallest_first_remove_modus",
        "filter_keep_key",
    ],
)
def test_filter_endpoint(data_fixture_name, request):
    client = TestClient(app)
    input_output: TestUtilInputOutput = request.getfixturevalue(data_fixture_name)
    input_rest = input_output.filter_input.convert_to_rest()

    # ToDo: We have to stringify the enum
    body = input_rest.dict()
    body["selection_modus"] = input_rest.selection_modus.value
    body["keep_keys"] = [str(i) for i in input_rest.keep_keys]
    response = client.put("/filter_polygons", json=body)
    assert response.status_code == 200
    algo_out = OverlapResult(**response.json())
    assert set([str(i) for i in algo_out.keep]) == set(
        [str(i) for i in input_output.filter_output.keep]
    )
    assert set([str(i) for i in algo_out.remove]) == set(
        [str(i) for i in input_output.filter_output.remove]
    )


def test_overlap_endpoint():
    client = TestClient(app)
    first_polygon = [i.dict() for i in convert_shapely_to_polygon(Polygon.from_bounds(0, 0, 2, 2))]
    second_polygon = [i.dict() for i in convert_shapely_to_polygon(Polygon.from_bounds(1, 1, 3, 3))]
    third_polygon = [i.dict() for i in convert_shapely_to_polygon(Polygon.from_bounds(1, 1, 3, 3))]

    response = client.put(
        "/calculate_overlap_statistics", json={"area": [first_polygon], "polygon": second_polygon}
    )
    assert response.status_code == 200
    out = OverlapStatistics(**response.json())
    assert out.overlap_area == 1
    assert out.non_overlap_area == 3
    assert out.is_touching

    response = client.put(
        "/calculate_overlap_statistics",
        json={"area": [first_polygon, second_polygon], "polygon": third_polygon},
    )
    assert response.status_code == 200
    out = OverlapStatistics(**response.json())
    assert out.overlap_area == 4
    assert out.non_overlap_area == 0
    assert out.is_touching


def create_rectangle(x, y, w, h):
    return [
        PointRest(lat=x, lon=y).dict(),
        PointRest(lat=x, lon=y + h).dict(),
        PointRest(lat=x + w, lon=y + h).dict(),
        PointRest(lat=x + w, lon=y).dict(),
    ]


def test_lp_optimization_endpoint(images):
    client = TestClient(app)
    rest_input = {"images": images, "num_images": 3, "threshold": 0.9, "keep_keys": ["3"]}

    response = client.put("/filter_images_lp_optimization", json=rest_input)
    assert response.status_code == 200
    out = response.json()

    assert len(out) == 3
    assert "3" in out
    assert "8" in out
    assert "1" not in out


def test_greedy_endpoint(images):
    client = TestClient(app)
    rest_input = {"images": images, "threshold": 0.9, "keep_keys": ["3"]}
    response = client.put("/filter_greedy", json=rest_input)
    assert response.status_code == 200
    out = response.json()

    assert len(out) > 3
    assert "3" in out
    assert "8" in out
    assert "0" in out


def test_greedy_endpoint_simple_filter_input(images):
    client = TestClient(app)
    images = {
        f"{1}": {
            "image_area": create_rectangle(
                0,
                0,
                1,
                1,
            ),
            "datetime_original": "2010-10-12T12:31",
        },
        f"{2}": {
            "image_area": create_rectangle(
                0,
                0,
                1,
                1,
            ),
            "datetime_original": "2010-10-12T12:30",
        },
    }
    rest_input = {"images": images, "threshold": 0.9}
    response = client.put("/filter_greedy", json=rest_input)
    assert response.status_code == 200
    out = response.json()

    assert len(out) == 1
    assert "2" in out
