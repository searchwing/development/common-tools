import pytest

from image_priorization.geometric_utils import Polygon, calculate_polygon_area
from image_priorization.structures import PointRest, convert_polygon_to_shapely


@pytest.mark.skip(reason="Values not valid anymore for non-sphere implementation")
def test_area_haus_der_statistic():
    # from google maps
    points = [
        (52.52239283819731, 13.417541603966349),
        (52.521955461741484, 13.419022183392393),
        (52.52343730166935, 13.420213084235082),
        (52.52378980248609, 13.419172387102282),
    ]
    area = 18490.65

    poly = Polygon([(i[1], i[0]) for i in points])
    out_area = calculate_polygon_area(poly)

    assert abs(area - out_area) / area < 0.05


@pytest.mark.skip(reason="Values not valid anymore for non-sphere implementation")
def test_area_helgoland():
    # form google maps
    points = [
        (54.187946745862604, 7.869863185537526),
        (54.1720735593916, 7.8893896678230355),
        (54.176243372421744, 7.897071514700193),
        (54.184355719588524, 7.8910633663046506),
        (54.18950359738631, 7.883295688736129),
    ]

    poly = Polygon([(i[1], i[0]) for i in points])
    out_area = calculate_polygon_area(poly)
    area = 1.59e6  # 1,59 km²

    assert abs(area - out_area) / area < 0.05


@pytest.mark.skip(reason="Values not valid anymore for non-sphere implementation")
def test_area_helgoland_points_rest():
    # form google maps
    points = [
        PointRest(lat=54.187946745862604, lon=7.869863185537526),
        PointRest(lat=54.1720735593916, lon=7.8893896678230355),
        PointRest(lat=54.176243372421744, lon=7.897071514700193),
        PointRest(lat=54.184355719588524, lon=7.8910633663046506),
        PointRest(lat=54.18950359738631, lon=7.883295688736129),
    ]

    out_area = calculate_polygon_area(convert_polygon_to_shapely(points))
    area = 1.59e6  # 1,59 km²

    assert abs(area - out_area) / area < 0.05
