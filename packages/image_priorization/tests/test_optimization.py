import random
import time
from datetime import datetime

import pytest
from pulp import LpBinary, LpConstraint, LpMaximize, LpProblem, LpStatus, LpVariable, lpSum, value

from image_priorization.lp_optimization import filter_images_lp_optimization
from image_priorization.structures import ImageWithDetections, PointRest


def test_basic_milp_solver():
    prob = LpProblem("AreaMaximuzatinProblem", LpMaximize)
    x1 = LpVariable("x1", 0, 1, cat=LpBinary)
    x2 = LpVariable("x2", 0, 1, cat=LpBinary)
    x3 = LpVariable("x3", 0, 1, cat=LpBinary)
    prob += x1 * 0.5 + x2 * 0.3 - x3 * 0.4
    status = prob.solve()
    assert LpStatus[status] == "Optimal"
    assert value(x1) == 1
    assert value(x2) == 1
    assert value(x3) == 0


def test_constraint_milp_solver():
    prob = LpProblem("AreaMaximuzatinProblem", LpMaximize)
    x1 = LpVariable("x1", 0, 1, cat=LpBinary)
    x2 = LpVariable("x2", 0, 1, cat=LpBinary)
    x3 = LpVariable("x3", 0, 1, cat=LpBinary)
    prob += lpSum([x1 * 0.5, x2 * 0.3, -x3 * 0.4])
    c = 1

    prob += lpSum([x1, x2, -c * x3]) >= c - 2
    prob += lpSum([c * (1 - x1), c * (1 - x2), x3]) >= 1

    status = prob.solve()
    assert LpStatus[status] == "Optimal"
    assert value(x1) == 1
    assert value(x2) == 0
    assert value(x3) == 0


def test_constraint_always_present_milp_solver():
    prob = LpProblem("AreaMaximuzatinProblem", LpMaximize)
    x1 = LpVariable("x1", 0, 1, cat=LpBinary)
    x2 = LpVariable("x2", 0, 1, cat=LpBinary)
    x3 = LpVariable("x3", 0, 1, cat=LpBinary)
    x4 = LpVariable("x4", 0, 1, cat=LpBinary)
    prob += lpSum([x1 * 0.5, x2 * 0.3, -x3 * 0.4, -10 * x4])
    c = 1

    prob += lpSum([x1, x2, -c * x3]) >= c - 2
    prob += lpSum([c * (1 - x1), c * (1 - x2), x3]) >= 1

    prob += lpSum([x4]) == 1

    status = prob.solve()
    assert LpStatus[status] == "Optimal"
    assert value(x1) == 1
    assert value(x2) == 0
    assert value(x3) == 0
    assert value(x4) == 1


def create_rectangle(x, y, w, h):
    return [
        PointRest(lat=x, lon=y),
        PointRest(lat=x, lon=y + h),
        PointRest(lat=x + w, lon=y + h),
        PointRest(lat=x + w, lon=y),
    ]


def test_filter_optimized_images():
    keep = filter_images_lp_optimization(
        {
            2: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 0.5, 0.5),
                datetime_original=datetime(1, 1, 1),
            ),
        },
        num_images=1,
    )
    assert len(keep) == 1
    assert keep[0] == 2


def test_filter_optimized_images_keep_images():
    keep = filter_images_lp_optimization(
        {
            2: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            1: ImageWithDetections(
                image_area=create_rectangle(0, 0, 0.5, 0.5),
                datetime_original=datetime(1, 1, 1),
            ),
        },
        num_images=1,
        keep_keys=[1],
    )
    assert len(keep) == 1
    assert keep[0] == 1


def test_filter_optimized_3_images():
    keep = filter_images_lp_optimization(
        {
            2: ImageWithDetections(
                image_area=create_rectangle(0, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            1: ImageWithDetections(
                image_area=create_rectangle(0.5, 0.5, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
            3: ImageWithDetections(
                image_area=create_rectangle(0.5, 0, 1, 1),
                datetime_original=datetime(1, 1, 1),
            ),
        },
        num_images=2,
        keep_keys=[],
    )
    assert len(keep) == 2
    assert set(keep) == set([1, 2])


# Note: More than 100 is a challenge for the solver
@pytest.mark.parametrize("n_images", [10, 100])
def test_filter_optimized_random_n_images(n_images):
    images = {
        k: ImageWithDetections(
            image_area=create_rectangle(
                random.uniform(0, 10),
                random.uniform(0, 10),
                random.uniform(0.01, 2),
                random.uniform(0.01, 2),
            ),
            datetime_original=datetime(1, 1, 1),
        )
        for k in range(n_images)
    }

    start = time.perf_counter()
    keep = filter_images_lp_optimization(
        images,
        num_images=n_images * 0.1,
        keep_keys=[],
    )
    stop = time.perf_counter()
    assert len(keep) == n_images * 0.1
    assert (stop - start) < 2


@pytest.mark.skip(reason="The current implementation is too slow")
def test_example_realistic_dataset(images_with_detections):
    assert len(images_with_detections) > 100

    start = time.time()
    keep = filter_images_lp_optimization(
        images_with_detections, num_images=len(images_with_detections) // 10
    )
    stop = time.time()

    assert (stop - start) / len(images_with_detections) < 1e-3
