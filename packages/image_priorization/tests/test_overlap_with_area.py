import pytest

from image_priorization import AreaCollector, OverlapStatistics
from image_priorization.structures import Polygon


def test_empty_polygon_exception_raised():
    ac = AreaCollector()
    with pytest.raises(ValueError):
        ac.calculate_polygon_set_statistics(Polygon.from_bounds(0, 0, 2, 2))


def test_area_collector():
    ac = AreaCollector()
    ac.add_area(Polygon.from_bounds(0, 0, 2, 2))

    out = ac.calculate_polygon_set_statistics(Polygon.from_bounds(1, 1, 3, 3))
    assert isinstance(out, OverlapStatistics)
    assert out.overlap_area == 1
    assert out.non_overlap_area == 3
    assert out.is_touching

    ac.add_area(Polygon.from_bounds(1, 1, 3, 3))
    out = ac.calculate_polygon_set_statistics(Polygon.from_bounds(1, 1, 3, 3))
    assert isinstance(out, OverlapStatistics)
    assert out.overlap_area == 4
    assert out.non_overlap_area == 0
    assert out.is_touching


def test_area_touching():
    ac = AreaCollector()
    ac.add_area(Polygon.from_bounds(0, 0, 2, 2))

    out = ac.calculate_polygon_set_statistics(Polygon.from_bounds(2, 2, 3, 3))
    assert isinstance(out, OverlapStatistics)
    assert out.overlap_area == 0
    assert out.non_overlap_area == 1
    assert out.is_touching

    out = ac.calculate_polygon_set_statistics(Polygon.from_bounds(0, 2, 3, 3))
    assert isinstance(out, OverlapStatistics)
    assert out.overlap_area == 0
    assert out.non_overlap_area == 3
    assert out.is_touching

    out = ac.calculate_polygon_set_statistics(Polygon.from_bounds(2.5, 2.5, 3, 3))
    assert isinstance(out, OverlapStatistics)
    assert out.overlap_area == 0
    assert out.non_overlap_area == 0.25
    assert not out.is_touching
