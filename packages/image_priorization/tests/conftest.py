import json
from pathlib import Path

import pytest
from pydantic import BaseModel

from image_priorization.structures import (
    ImageWithDetections,
    OverlapResult,
    Polygon,
    PolygonFilterInput,
    SelectionModus,
    TestUtilInputOutput,
)


@pytest.fixture()
def filter_two_overlapping():
    return TestUtilInputOutput(
        filter_input=PolygonFilterInput(
            polygons={
                1: Polygon.from_bounds(0, 0, 2, 2),
                2: Polygon.from_bounds(0.5, 0.5, 1.5, 1.5),
            }
        ),
        filter_output=OverlapResult(remove=[2], keep=[1]),
    )


@pytest.fixture()
def filter_two_non_overlapping_polygons():
    return TestUtilInputOutput(
        filter_input=PolygonFilterInput(
            polygons={
                1: Polygon.from_bounds(0, 0, 2, 2),
                2: Polygon.from_bounds(10, 10, 15, 15),
            }
        ),
        filter_output=OverlapResult(remove=[], keep=[1, 2]),
    )


@pytest.fixture()
def filter_three_overlapping_polygons():
    return TestUtilInputOutput(
        filter_input=PolygonFilterInput(
            polygons={
                1: Polygon.from_bounds(0, 0, 2, 2),
                2: Polygon.from_bounds(0, 2, 2, 4),
                3: Polygon.from_bounds(1, 1, 2, 3),
            }
        ),
        filter_output=OverlapResult(remove=[3], keep=[1, 2]),
    )


@pytest.fixture()
def filter_standard_remove_modus():
    return TestUtilInputOutput(
        filter_input=PolygonFilterInput(
            polygons={
                1: Polygon.from_bounds(0, 0, 2, 2),
                2: Polygon.from_bounds(0, 0, 2, 1),
                3: Polygon.from_bounds(0, 1, 2, 2),
            }
        ),
        filter_output=OverlapResult(remove=[1], keep=[2, 3]),
    )


@pytest.fixture()
def filter_largest_first_remove_modus():
    return TestUtilInputOutput(
        filter_input=PolygonFilterInput(
            polygons={
                1: Polygon.from_bounds(0, 0, 2, 2),
                2: Polygon.from_bounds(0, 0, 2, 1),
                3: Polygon.from_bounds(0, 1, 2, 2),
            },
            selection_modus=SelectionModus.LARGEST_FIRST,
        ),
        filter_output=OverlapResult(remove=[1], keep=[2, 3]),
    )


@pytest.fixture()
def filter_smallest_first_remove_modus():
    return TestUtilInputOutput(
        filter_input=PolygonFilterInput(
            polygons={
                1: Polygon.from_bounds(0, 0, 2, 2),
                2: Polygon.from_bounds(0, 0, 2, 1),
                3: Polygon.from_bounds(0, 1, 2, 2),
            },
            selection_modus=SelectionModus.SMALLEST_FIRST,
        ),
        filter_output=OverlapResult(remove=[2, 3], keep=[1]),
    )


@pytest.fixture()
def filter_keep_key():
    return TestUtilInputOutput(
        filter_input=PolygonFilterInput(
            polygons={
                1: Polygon.from_bounds(0, 0, 2, 2),
                2: Polygon.from_bounds(0, 0, 2, 1),
                3: Polygon.from_bounds(0, 1, 2, 2),
            },
            selection_modus=SelectionModus.SMALLEST_FIRST,
            keep_keys=[3],
        ),
        filter_output=OverlapResult(remove=[2], keep=[1, 3]),
    )


@pytest.fixture()
def images_with_detections():
    file = Path(Path(__file__).parent, "assets", "images_with_detection.json")
    with open(file) as fio:
        images_with_detections = json.load(fio)
    return {i: ImageWithDetections(**data) for i, data in enumerate(images_with_detections)}
