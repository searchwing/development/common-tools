import json
from pathlib import Path
from typing import Dict

from PIL import Image
from PIL.ExifTags import TAGS

from image_priorization.structures import (
    ImageCorners,
    ImageWithDetections,
    convert_imagecrns_to_shapely,
    convert_shapely_to_polygon,
)


def extract_metadata(exif_data):
    output_dict = {}

    for tag_id in exif_data:
        tag = TAGS.get(tag_id, tag_id)
        content = exif_data.get(tag_id)
        output_dict[tag] = content
    return output_dict


def extract_metadata_from_image(file: Path) -> Dict:
    im = Image.open(file.absolute())
    exif = im.getexif()

    image_meta = extract_metadata(exif)
    image_meta.update(extract_metadata(exif.get_ifd(0x8769)))

    simplified_uc = "UserCommentSimplified"
    image_meta[simplified_uc] = json.loads(image_meta["UserComment"][8:].decode("ASCII"))

    if "imgcrns" in image_meta[simplified_uc]:
        for key, value in image_meta[simplified_uc]["imgcrns"].items():
            image_meta[simplified_uc]["imgcrns"][key] = [float(i) for i in value.split(",")]
        image_meta["ImageCrns"] = ImageCorners(**image_meta["UserCommentSimplified"]["imgcrns"])
        if image_meta["ImageCrns"].is_valid():
            image_meta["Polygon"] = convert_imagecrns_to_shapely(image_meta["ImageCrns"])
    return image_meta


def get_metadata_for_images_in_folder(folder: Path):
    images_metadata = {}
    for image_file in folder.glob("**/*.jpg"):
        images_metadata[image_file.stem] = extract_metadata_from_image(Path(image_file))
    images_with_detection = {}
    for k, v in images_metadata.items():
        if "Polygon" in v:
            datestring = (
                v["DateTimeOriginal"][0:10].replace(":", "-")
                + "T"
                + v["DateTimeOriginal"][11:]
                + "."
                + v["SubsecTimeOriginal"]
            )
            images_with_detection[k] = ImageWithDetections(
                **{
                    "datetime_original": datestring,
                    "detections": [],  # ToDo
                    "image_area": convert_shapely_to_polygon(v["Polygon"]),
                }
            )
    return images_metadata, images_with_detection
