# Searchwing Image Priorization

This package includes basic functionality for image priorization algorithms.

The packages currently exposes only three functions:
* `filter_overlapping_polygons`: remove polygons, which are completely overlapping (Use-case: Filter out images with no additional information)
* `AreaCollector`: calculate overlaps of new polygons to a bunch of polygons (Use-case: Priorization of live video transmitting)
* `filter_images_lp_optimization`: filter images by solving an lp optimization problem
* `queue_greedy_priorization`: A prioretized queue which uses image surface geometry for prioritization. To be used on the plane with quite some heuristics to make it runnable on the pi.


## Usage
### Local checkout
Checkout this repository first.
The package itself comes in two flavors:
* standard `pip install .` for usage in python
* server `pip install .[server]` for usage as a rest-server

### From Git
If you only want to have the package:
* `pip install "git+https://gitlab.com/searchwing/development/common-tools.git#egg=image_priorization&subdirectory=packages/image_priorization"`
* `pip install "git+https://gitlab.com/searchwing/development/common-tools.git#egg=image_priorization[server]&subdirectory=packages/image_priorization"`

Note to include `@<tag/branch>` after `.git` if you want to install a specific branch.
(In future, we should tag the package correctly for a clean installation)

## Development
Unfortunately there is no gitlab precommit linter. However, we check the linting in the pipeline. Therefore make sure to run `dev/code_checker.sh` before commiting.

### Setup
* `pip install -r requirements_dev.txt`
* `pip install -e .[server]`

or simply use the devcontainer 

### Testing
Simple testing can be fone with `pytest .` in the main directory. If you want to check the package run `tox`.


### Docker support
The easiest way to run the server is by using the dockerfile:
* `docker build -t image_priorization .`
* `docker run --rm -p 5000:5000 image_priorization`

In order to create a container and push it to gitlab (make sure you logged in via `docker login registry.gitlab.com`)
* `docker build -t registry.gitlab.com/searchwing/development/common-tools/image_priorization_server:<version_of_package>`
* `docker push registry.gitlab.com/searchwing/development/common-tools/image_priorization_server:0.0.1`